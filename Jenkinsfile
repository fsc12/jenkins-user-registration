#!groovy
def majorVersionNumber = 1.0
def version  = "${majorVersionNumber}.${env.BUILD_NUMBER}"
def buildInfo = Artifactory.newBuildInfo()

node {
  stage('Checkout') {
     git url: 'https://fsc12@bitbucket.org/fsc12/user-registration-v2.git'
  }  
  stage('SonarQube analysis') {
    withSonarQubeEnv('sonar5.6') {
      sh "mvn versions:set -DnewVersion=${version}"
      sh "mvn -f user-registration-application/pom.xml  org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar"
    }
  }  
  
  
  stage("Quality Gate"){
// Check if coverate threshold is met, otherwise fail the job
            def response = httpRequest 'http://sonar111.westeurope.cloudapp.azure.com:9000/api/qualitygates/project_status?projectKey=com.ewolff:user-registration-application'
            def slurper = new groovy.json.JsonSlurper()
            def result = slurper.parseText(response.content)

            if (result.projectStatus.status == "ERROR") {
               currentBuild.result = 'FAILURE' 
               error "Pipeline aborted due to quality gate failure."
            }  
  }  
  
  stage('Build') {
      sh "mvn versions:set -DnewVersion=${version}"
      sh "mvn -f user-registration-application/pom.xml clean package"
      buildInfo.env.capture = true
      buildInfo.env.collect()
      junit allowEmptyResults: true, testResults: '**/target/**/TEST*.xml'
      sh "rm -f docker/user-registration/*.war"
      sh "cp user-registration-application/target/user-registration-application-${version}.war  ./docker/user-registration/user-registration-application-latest.war"

  }
  
  stage('Upload Repo') {
      def server = Artifactory.server "repo111"
      def uploadSpec = """{
          "files": [
              {
              "pattern": "user-registration-application/target/user*.jar",
              "target": "libs-release/com/ewolff/user-registration-application/${version}/",
              "recursive": "false"
              }
           ]
        }"""
      def uploadPom = """{
          "files": [
              {
              "pattern": "user-registration-application/pom.xml",
              "target": "libs-release/com/ewolff/user-registration-application/${version}/user-registration-application-${version}.pom",
              "recursive": "false"
              }
           ]
        }"""
      def uploadParentPom = """{
          "files": [
              {
              "pattern": "pom.xml",
              "target": "libs-release/com/ewolff/user-registration/${version}/user-registration-${version}.pom",
              "recursive": "false"
              }
           ]
        }"""
      def buildInfoUploadPom = server.upload(uploadPom)    
      def buildInfoUploadParentPom = server.upload(uploadParentPom)    
      def buildInfoUploadSpec = server.upload(uploadSpec)
      
      buildInfo.append(buildInfoUploadSpec)
      buildInfo.append(buildInfoUploadPom)
      buildInfo.append(buildInfoUploadParentPom)
      server.publishBuildInfo(buildInfo)
      
  }
  
      
  stage('Int-Tests') {
      sh "mvn -f user-registration-acceptancetest-jbehave/pom.xml integration-test"
      sh "mvn -f  user-registration-acceptancetest-selenium/pom.xml test"
  }	   
	   
  stage ('Build/Push docker-Image')  {
      dir('./docker/user-registration') {
         def appImg = docker.build("vkbtest2/user-registration:${version}")
         docker.withRegistry('https://index.docker.io/v1/', 'dockerhub1') {
         appImg.push();
         }
      }
   }

   stage ('Deploy to Acc-Stage') {
      try {
         sh "kubectl delete deployment user-registration --kubeconfig  /home/vkbadmin/.kube/config"
      } catch (err) {
        echo "Kein deployment user-registration vorhanden."
      }
      try {
         sh "kubectl delete svc  user-registration-web   --kubeconfig  /home/vkbadmin/.kube/config"
      } catch (err) {
         echo "Kein Service user-registration-web vorhanden."
      }
      sh "kubectl run user-registration  --kubeconfig  /home/vkbadmin/.kube/config  --image=vkbtest2/user-registration:${version} --port=8088"
      sh "kubectl expose deployment user-registration --kubeconfig  /home/vkbadmin/.kube/config --port=8088  --target-port=8088  --name=user-registration-web   --type=LoadBalancer"
  }      


}

